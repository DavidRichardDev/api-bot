# api-bot

Api for management of bots and messages with users

## Clone Repository

git clone https://gitlab.com/DavidRichardDev/api-bot.git


## Requirements

- nodejs
- yarn
- mongodb

## Run commands

- yarn

## Run application

- yarn dev

## Run test

- yarn test

## Generate pack deploy

- yarn build

On root of this project has docPT.pdf and docEN.pdf with explanation about the challenge.