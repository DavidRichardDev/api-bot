import { Router } from "express";
import BotController from "@controllers/BotController";
import ClientController from "@controllers/ClientController";
import MessageController from "@controllers/MessageController";

const router = Router();

//Bots
router.get('/bots', BotController.index);
router.post('/bots', BotController.store);
router.put('/bots/:id', BotController.update);
router.delete('/bots/:id', BotController.destroy);

//Clients
router.get('/clients', ClientController.index);
router.post('/clients', ClientController.store);

//Messages
router.get('/messages', MessageController.index);
router.get('/messages/:id', MessageController.show);
router.post('/messages', MessageController.store);

export { router }