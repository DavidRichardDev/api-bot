import { Bot } from 'src/schemas/Bot';

test('it should be ok', () => {
  const bot = new Bot();

  bot.name = 'Bot One';

  expect(bot.name).toEqual('Bot One');
})
