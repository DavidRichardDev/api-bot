import { Document, SchemaTimestampsConfig } from 'mongoose';

export interface MessageInterface extends Document{
    conversationId: String,
    from: String,
    to: String,
    text: String
}
  