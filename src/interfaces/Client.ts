import { Document } from 'mongoose';

export interface ClientInterface extends Document{
    conversationId: String
}
  