import { Request, Response } from 'express'
import { Message } from '@schemas/Message'

class MessageController {

    public async index (req: Request, res: Response): Promise<Response> {
        const { conversationId } = req.query;
        let query = {};
        if(conversationId){
            query = { conversationId: conversationId };
        }

        const messages = await Message.find({}, query);

        return res.json(messages);
    }

    public async show (req: Request, res: Response): Promise<Response> {
        const message = await Message.findById(req.params.id);

        return res.json(message);
    }
    
    public async store (req: Request, res: Response): Promise<Response> {
        const message = await Message.create(req.body);

        return res.json(message);
    }
}

export default new MessageController();