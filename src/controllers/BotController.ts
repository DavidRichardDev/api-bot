import { Request, Response } from 'express'
import { Bot } from '@schemas/Bot'

class BotController {

    public async index (req: Request, res: Response): Promise<Response> {
        const bots = await Bot.find();

        return res.json(bots);        
    }
    
    public async store (req: Request, res: Response): Promise<Response> {
        const bot = await Bot.create(req.body);

        return res.json(bot);        
    }

    public async update (req: Request, res: Response): Promise<Response> {
        const bot = await Bot.findByIdAndUpdate(req.params.id, req.body, {new: true});
        
        return res.json(bot);
    }

    public async destroy (req: Request, res: Response): Promise<Response> {
        await Bot.findByIdAndRemove(req.params.id);
    
        return res.send();
    }
}

export default new BotController();