import { Request, Response } from 'express'
import { Client } from '@schemas/Client'
import { uuid } from 'uuidv4';


class ClientController {

    public async index (req: Request, res: Response): Promise<Response> {
        const clients = await Client.find();

        return res.json(clients);        
    }
    
    public async store (req: Request, res: Response): Promise<Response> {
        const hash =  uuid();
        
        const client = await Client.create({"conversationId":hash});
        
        return res.json(client);
    }
}

export default new ClientController();