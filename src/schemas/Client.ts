import { Document, Schema, Model, model } from 'mongoose';
import { ClientInterface } from '@interfaces/Client';

export interface ClientModel extends ClientInterface {
    conversationId: String,
}

const ClientSchema = new Schema({
    conversationId: {
        type: String,
        unique: true,
        required: true,
    }
  }, {
    timestamps: true
});
  
export const Client: Model<ClientModel> = model<ClientModel>('Client', ClientSchema);
  