import { Document, Schema, Model, model } from 'mongoose';
import { BotInterface } from '@interfaces/Bot';

export interface BotModel extends BotInterface {
    name: String,
    nameBot(): String
}

const BotSchema = new Schema({
    name: {
        type: String,
        required: true
    }
  }, {
    timestamps: true
});

BotSchema.methods.nameBot = function (): String {
    return (this.name.trim());
}
  
export const Bot: Model<BotModel> = model<BotModel>('Bot', BotSchema);
  