import { Document, Schema, Model, model } from 'mongoose';
import { MessageInterface } from '@interfaces/Message';

export interface MessageModel extends MessageInterface {
    
}

const MessageSchema = new Schema({
    conversationId: {
        type: String
    },
    from:{
        type: String,
        required: true
    },
    to:{
        type: String
    },
    text:{
        type: String,
        required: true
    }
    
  }, {
    timestamps: true
});
  
export const Message: Model<MessageModel> = model<MessageModel>('Message', MessageSchema);
  